__copyrights__ = "Will Dabney, Pyrl https://github.com/amarack/python-rl/blob/master/pyrl/basis/fourier.py"
__author__ = ["Will Dabney"]

import numpy, itertools


class TrivialBasis:
    """Uses the features themselves as a basis. However, does a little bit of basic manipulation
    to make things more reasonable. Specifically, this allows (defaults to) rescaling to be in the
    range [-1, +1].
    """

    def __init__(self, nvars, ranges):
        self.numTerms = nvars
        self.ranges = numpy.array(ranges)

    def scale(self, value, pos):
        if self.ranges[pos,0] == self.ranges[pos,1]:
            return 0.0
        else:
            return (value - self.ranges[pos,0]) / (self.ranges[pos,1] - self.ranges[pos,0])

    def getNumBasisFunctions(self):
        return int(self.numTerms)

    def __call__(self, features):
        if len(features) == 0:
            return numpy.ones((1,))
        return (numpy.array([self.scale(features[i],i) for i in range(len(features))]) - 0.5)*2.


class FourierBasis(TrivialBasis):
    """Fourier Basis linear function approximation. Requires the ranges for each dimension, and is thus able to
    use only sine or cosine (and uses cosine). So, this has half the coefficients that a full Fourier approximation
    would use.
    From the paper:
    G.D. Konidaris, S. Osentoski and P.S. Thomas.
    Value Function Approximation in Reinforcement Learning using the Fourier Basis.
    In Proceedings of the Twenty-Fifth Conference on Artificial Intelligence, pages 380-385, August 2011.
    """

    def __init__(self, nvars, ranges, order=3):
        nterms = pow(order + 1.0, nvars)
        self.numTerms = int(nterms)
        self.order = order
        self.ranges = numpy.array(ranges)
        iter = itertools.product(range(order+1), repeat=nvars)
        self.multipliers = numpy.array([list(map(int,x)) for x in iter])

    def __call__(self, features):
        if len(features) == 0:
            return numpy.ones((1,))
        basisFeatures = numpy.array([self.scale(features[i],i) for i in range(len(features))])
        return numpy.cos(numpy.pi * numpy.dot(self.multipliers, basisFeatures))

    def __len__(self):
        return self.numTerms

if __name__ == "__main__":
    features = FourierBasis(1, ((0,4),))
    print(features([3,]))
