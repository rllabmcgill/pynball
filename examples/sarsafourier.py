from pinball import PinballModel
from fourier import FourierBasis
import numpy as np

lr = 1e-3
norder = 3
epsilon = 1e-2
discount = 0.99

ntrials = 1
nepisodes = 250
max_steps = 5000

rng = np.random.RandomState(2468)

environment = PinballModel('pinball_simple_single.cfg')
legal_actions = [
    PinballModel.ACC_X,
    PinballModel.DEC_Y,
    PinballModel.DEC_X,
    PinballModel.ACC_Y,
    PinballModel.ACC_NONE]

def egreedy(phi, weights):
     if rng.uniform() < epsilon:
         return rng.choice(legal_actions)
     return np.argmax(np.dot(weights.T, phi))

for trial in range(ntrials):
    features = FourierBasis(4, [[0.0, 1.0], [0.0, 1.0], [-2.0, 2.0], [-2.0, 2.0]], order=norder)
    nfeatures, nactions = features.getNumBasisFunctions(), len(legal_actions)
    weights = np.zeros((nfeatures, nactions))

    for i in range(nepisodes):
        environment = PinballModel('pinball_simple_single.cfg')

        phi = features(environment.get_state())
        action = egreedy(phi, weights)
        valueprev = 0.

        nsteps = 0
        cumreward = 0.
        while not environment.episode_ended() and nsteps < max_steps:
          reward = environment.take_action(action)
          phiprime = features(environment.get_state())
          actionprime = egreedy(phiprime, weights)

          tderror = reward - valueprev
          if not environment.episode_ended():
              value = np.dot(weights[:, actionprime].T, phiprime)
              tderror += value
          weights[:, action] += lr*tderror*phi

          valueprev = value
          action = actionprime
          phi = phiprime
          nsteps += 1
          cumreward += reward
        print('Episode {} Return: {} Nsteps: {}'.format(i, cumreward, nsteps))
