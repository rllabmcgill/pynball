# Pynball

![](screenshot.jpg)

Pypinball is a Python implementation of the Pinball domain for reinforcement learning
initially introduced in [1].

The code structure and coefficients closely ressemble the original Java implementation
available at http://www-all.cs.umass.edu/~gdk/pinball/. Therefore, the dynamics are
not expected to be any different from those described in Konidaris' work.

    [1] G.D. Konidaris and A.G. Barto. Skill Discovery in Continuous Reinforcement
    Learning Domains using Skill Chaining. Advances in Neural Information Processing
    Systems 22, pages 1015-1023, December 2009.

# Example

An example of SARSA(0) using Fourier basis:

```
PYTHONPATH=$PYTHONPATH:$PWD python examples/sarsafourier.py
```

Note that for a more permanent *installation*, you should set the
directory containing the pinball domain in your `PYTHONPATH`. For example, you
can add the following to your `.bashrc`:

```
export PYTHONPATH=$PYTHONPATH:$HOME/workspace/pynball/
```

# License

    Copyright (C) 2013  Pierre-Luc Bacon

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
